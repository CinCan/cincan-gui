### CinCan GUI

CinCan GUI is a web-based graphical interface for the Concourse analysis pipelines we have been working on. The main functionality of it is to present all the data and meta data collected with the analysis tools, as well as all the different tooling pipelines available on the user's system.

#### Functionality

The main motivation with developing this user interface came from the fact, that all data and results in Concourse pipelines are just files scattered in a file system without any context or connection to where those files came from and what the significance of those files are. Although the user running the pipelines would understand that, sharing the results and explaining where they came from would cause extra work that could be automated.

##### Concourse pipelines

Concourse is a continuous integration software that works around git repositories. When you upload a file to a git repository connected to the CI system, it will perform operations on that file based on its configuration and return any resulting data to that repository. The result could be as simple as the filetype of the sample file or as complex as extracted source code. These results themselves lack any context or meta-data related to the analysis, so we must create and keep track of it ourselves. 

With the CinCan GUI this is achieved simply with using pre-planned folder structure. The repository that the GUI runs around contains four folders: samples, results, pipelines and tools. Samples-folder contains the samples that the system will process, results-folder has sub-folders for all the samples, and those sub-folders have sub-folders for all the tools that have been used to analyze the sample, containing all the resulting data from that particular tool. Pipelines-folder has configuration files for different pipelines available in the system, and tools-folder has scripts used to execute those tools in the pipelines.

##### Dashboard

Dashboard is the main page of the user interface providing a quick overview of the system. It lists the latest samples and results in the system, alongside with information of where the results came from and how many results there are from a spesific sample file as well as the tools and pipelines available in the system. This offers a jumping point to different sub-sections of the system.

##### Samples

Samples are any files that the user wants to analyze. Samples-page lists all the samples in the system with a quick preview of all the metadata about them, like resulting files from a particular sample and tools that have been used to analyze them. 

From here, user can navigate to individual samples' page containing an expanded view of the metadata. From here user can navigate to individual tools or results page.

##### Results

Results are divided to logs and artefacts. Logs are any report that the tools or pipelines have produced, and artefacts are any other files, such as extracted files, source code or even malware. Results-page lists all these on single page, similar to how samples are listed with a short preview of each one and a link to individual results page.

 Just as with samples, user can navigate to either the tools or samples page.

##### Tools

Tools-page provides a listing of all tools available in the system. From here user can navigate to each tools page containing a preview of the script used to run the tool in the pipeline and a listing of all samples analyzed with that particular tool and all the results from those analyzes.

##### Pipelines

Pipelines have their own listing page, as well as individual pages for each pipeline. These individual pages contain only a preview of the configuration file for that pipeline.

##### User Management

While this user interface is meant to be running in an internal network or locally on end-users computer, there is still a basic user management system. User profiles are divided to basic users and administrators, with basic users having access to the basic parts of the system and administrators having also access to user management and configuration page. Currently the system uses only sqlite for the user database, as the database needs for the system are rather basic, but it can be configured to use more complex database systems like MySQL or PostgreSQL if needed. Users' passwords are protected with bcrypt hash.

#### Limitations and future plans

Currently this interface is limited by the need for a spesific directory structure to produce the needed metadata. This could be amended with a broader configuration system allowing the user modify the structure and locations of the directories. After the creation of this system CinCan has also pivoted away from the Concourse CI system due to its limitations and towards our own CinCan command and Minion tools that provide similar functionality with more streamlined and faster process. The graphical interface could be modified to support these tools, but for now we have focused on developing the basic functionality of the tooling, and with Minion and CinCan command providing a better commandline interface experience compared to Concourse, the priority of having a graphical interface has been much lower.