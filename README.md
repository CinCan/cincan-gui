### CinCan Workspace Web Interface

Flask based user interface for managing input, output and tool scripts of the CinCan environment.

#### Setup from the source
The interface needs the following environment keys:

* `SECRET_KEY`: key Flask uses for encrypting the session.
* `DATABASE_URL`: path to the database file, defaults to `app.db` in app's root folder.
* `WORKSPACE_PATH`: path to the CinCan repository folder, defaults to `workspace` in app's root folder.

The interface starts with `flask run`.

On the first run-time, the app has only one account, username `root` and password `root`. 
You can add more users and change password from the 'User management'-page on the interface.
The interface can be found at http://localhost:5000

#### Running the Docker image
Pull the image:
```bash
docker pull cincan/cincan-gui:dev
```
Run the image:
```bash
docker run -p 5000:5000 \
-v /path/to/repository/on/host:/path/to/workspace \
-e WORKSPACE_PATH=/path/to/workspace \ cincan-gui
```
When running the docker image, we have to redirect a port to the port 5000 inside the image using the -p parameter. 
After that, the -v paramater mounts the workspace repository on the host to the workspace folder inside the container.
If you wish to modify the environment variables from the default values, you can do that with the -e parameter.
The interface can be found at http://localhost:5000