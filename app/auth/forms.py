from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, EqualTo


class LoginForm(FlaskForm):
    """ Login form """
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')

    submit = SubmitField('sign in')


class ChangePasswordForm(FlaskForm):
    """ Form for changing user password """
    old_password = PasswordField('Old password', validators=[DataRequired()])
    new_password1 = PasswordField('New password', validators=[DataRequired()])
    new_password2 = PasswordField('Repeat new password', validators=[DataRequired(), EqualTo('new_password1')])

    submit = SubmitField('change password')


class CreateAccountForm(FlaskForm):
    """ Account creation form """
    username = StringField('Username', validators=[DataRequired()])
    new_password1 = PasswordField('Password', validators=[DataRequired()])
    new_password2 = PasswordField('Repeat new password', validators=[DataRequired(), EqualTo('new_password1')])
    force_password_change = BooleanField('Force password change on login')
    admin = BooleanField('Admin')

    submit = SubmitField('add user')
