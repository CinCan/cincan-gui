import os
import datetime
import json
import hashlib
import filetype
import git

REPOSITORIES = {
    'results': 'results',
    'tools': 'master',
    'samples': 'sample-source'
    }

RESULT_FOLDERS = {
    'json_output': 'json-output',
    'tool_version': 'tool-version-info'
}


def calculate_SHA256(file_path):
    """ Calculate SHA256 hash from a given filepath"""
    sha256_hash = hashlib.sha256()
    with open(file_path, 'rb') as f:
        for block in iter(lambda: f.read(4096), b''):
            sha256_hash.update(block)

    return sha256_hash.hexdigest()


def date_from_timestamp(timestamp):
    """ Convert epoch timestamp to human readable form """
    return datetime.datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%dT%H:%M:%SZ')


def human_readable_size(size):
    """ Convert file size to human readable form """

    for unit in ['B', 'KB', 'MB', 'GB', 'TB']:
        if abs(size) < 1024.0:
            return '{:.2f}{}'.format(size, unit)
        size /= 1024.0
    return '{:.2f}{}'.format(size, 'PB')


def build_tree(tree):
    # TODO documentation
    """ Build a git tree in a list recursively from the target repository """
    folders = {}
    files = []

    for file in tree.blobs:
        files.append(file.path)

    for folder in tree.trees:
        folders[folder.path] = build_tree(folder)
    return folders, files


def collect_result_metadata(folders, results_path, repository_path):
    # TODO documentation
    """

    :param folders:
    :param results_path:
    :return:
    """

    collected_results = {}
    json_output_path = os.path.join(repository_path, RESULT_FOLDERS['json_output'])

    for tool_path in os.listdir(json_output_path):
        tool = tool_path + '.sh'
        tool_path = os.path.join(json_output_path, tool_path)
        for json_result_path in os.listdir(tool_path):
            json_result_path = os.path.join(tool_path, json_result_path)
            json_result_hash = calculate_SHA256(json_result_path)
            contents = None
            sample = None
            file_size = os.path.getsize(json_result_path)
            file_name = os.path.basename(json_result_path)
            modified = os.path.getmtime(json_result_path)
            extension = os.path.splitext(json_result_path)[1].lstrip('.')
            kind = filetype.guess(json_result_path)

            if extension in ['json']:
                with open(json_result_path, 'r') as f:
                    contents = f.read()
                    if len(contents) == 0:
                        contents = None
                    else:
                        contents = json.loads(contents)
                        try:
                            sample = contents['fileSHA256']
                        except KeyError:
                            sample = 'Could not be determined'
                        except TypeError:
                            sample = 'Could not be determined'

            if kind is not None:
                mime = kind.mime
            else:
                mime = 'Could not be determined'
            collected_results[json_result_hash] = {
                'hash': json_result_hash,
                'modified': date_from_timestamp(modified),
                'tool': tool,
                'sample': sample,
                'extension': extension,
                'MIME': mime,
                'contents': contents,
                'file_size': human_readable_size(file_size),
                'file_path': json_result_path,
                'file_name': file_name,
            }

    return collected_results


def collect_tool_metadata(folders, tools_path, repository_path):
    # TODO documentation
    """
    Collect metadata of available tool scripts in the environment.
    :param folders: future-proofing
    :param tools_path: root path of the tools branch
    :return:
    """
    collected_tools = {}

    for tool_path in tools_path:
        tool_path = os.path.join(repository_path, tool_path)
        tool_name = os.path.basename(tool_path)
        modified = os.path.getmtime(tool_path)
        tool_hash = calculate_SHA256(tool_path)
        tool_extension = os.path.splitext(tool_path)[1].lstrip('.')

        contents = []
        if tool_extension in ['sh']:
            with open(tool_path, 'r') as f:
                contents = f.readlines()
        tool = {
            'hash': tool_hash,
            'tool_name': tool_name,
            'modified': date_from_timestamp(modified),
            'results': [],
            'contents': contents,
            'script_path': tool_path,
            'config_path': None
        }

        collected_tools[tool_name] = tool

    return collected_tools


def collect_sample_metadata(folders, samples_path, repository_path):
    # TODO documentation
    """

    :param folders: future-proofing
    :param samples_path: root path of the sample branch
    :return:
    """
    collected_samples = {}

    for sample_path in samples_path:
        sample_path = os.path.join(repository_path, sample_path)
        sample_hash = calculate_SHA256(sample_path)
        modified = os.path.getmtime(sample_path)
        extension = os.path.splitext(sample_path)[1]
        file_size = os.path.getsize(sample_path)
        file_name = os.path.basename(sample_path)
        kind = filetype.guess(sample_path)

        if kind is not None:
            mime = kind.mime
        else:
            mime = 'Could not be determined'
        used_tools = {}

        collected_samples[sample_hash] = {
            'hash': sample_hash,
            'modified': date_from_timestamp(modified),
            'results': {},
            'extension': extension,
            'MIME': mime,
            'artefact_count': 0,
            'log_count': 0,
            'file_name': file_name,
            'file_size': human_readable_size(file_size),
            'file_path': sample_path,
        }

    return collected_samples


def connect_metadata(samples, tools, results):
    # TODO documentation
    """

    :param samples: Dictionary collection of samples
    :param tools: Dictionary collection of tools
    :param results:
    :return:
    """

    for result_hash, result_metadata in results.items():
        sample = result_metadata['sample']
        tool = result_metadata['tool']

        if sample in samples:
            if tool in samples[sample]['results'].keys():
                samples[sample]['results'][tool].append(result_hash)
            else:
                samples[sample]['results'][tool] = [result_hash]
        else:
            result_metadata['sample_not_found'] = result_metadata['sample']
            result_metadata['sample'] = None

        if tool in tools:
            tools[tool]['results'].append(result_hash)
        else:
            result_metadata['tool_not_found'] = result_metadata['tool']
            result_metadata['tool'] = None

    return samples, tools, results


def get_result_by_hash(metadata, result_hash):
    # TODO documentation
    """

    :param results:
    :param samples:
    :param tools:
    :param result_hash:
    :return:
    """

    tools = metadata['tools']
    samples = metadata['samples']
    results = metadata['results']
    result = results[result_hash]
    if result['sample'] is not None:
        sample = samples[result['sample']]
    else:
        sample = None
    if result['tool'] is not None:
        tool = tools[result['tool']]
    else:
        tool = None

    return result, sample, tool


def get_sample_by_hash(metadata, sample_hash):
    # TODO documentation
    """

    :param metadata:
    :param sample_hash:
    :return:
    """

    tools = metadata['tools']
    samples = metadata['samples']
    results = metadata['results']

    try:
        sample = samples[sample_hash]
    except KeyError:
        return None, None, None

    result_hashes = sample['results']
    sample_tools = {}
    sample_results = {}

    for result_hash in result_hashes:
        sample_result = results[result_hash]
        sample_results[result_hash] = sample_result
        tool_name = sample_result['tool']
        sample_tools[tool_name] = tools[tool_name]

    return sample, sample_tools, sample_results


def get_tool_by_name(metadata, tool_name):
    """

    :param metadata:
    :param tool_name:
    :return:
    """

    tools = metadata['tools']
    samples = metadata['samples']
    results = metadata['results']

    tool = tools[tool_name]
    result_hashes = tool['results']
    tool_samples = {}
    tool_results = {}

    for result_hash in result_hashes:
        tool_result = results[result_hash]
        tool_results[result_hash] = tool_result
        sample_hash = tool_result['sample']
        try:
            tool_samples[sample_hash] = samples[sample_hash]
        except KeyError:
            pass

    return tool, tool_samples, tool_results


def collect_environment_metadata(path_repository):
    """

    :param path_repository:
    :return:
    """
    repo = git.Repo(path_repository)
    branch_samples = REPOSITORIES['samples']
    branch_tools = REPOSITORIES['tools']
    branch_results = REPOSITORIES['results']
    branch_active = repo.active_branch

    repo.branches[branch_tools].checkout()
    tree_tools = repo.tree()
    folders_tools, path_tools = build_tree(tree_tools)

    tools = collect_tool_metadata(folders_tools, path_tools, path_repository)

    repo.branches[branch_results].checkout()
    tree_results = repo.tree()
    folders_results, path_results = build_tree(tree_results)

    results = collect_result_metadata(folders_results, path_results, path_repository)
    repo.branches[branch_samples].checkout()

    tree_samples = repo.tree()
    folders_samples, path_samples = build_tree(tree_samples)

    samples = collect_sample_metadata(folders_samples, path_samples, path_repository)
    samples, tools, results = connect_metadata(samples, tools, results)
    metadata = {
        "samples": samples,
        "pipelines": None,
        "tools": tools,
        "results": results
    }

    branch_active.checkout()

    return metadata

