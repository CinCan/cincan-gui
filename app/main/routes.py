import os

from flask import render_template, send_file, flash, abort
from app.main import bp
from app.database import db
from flask_login import login_required, current_user
from app.models import User
from app.main.forms import ChangeGitLabURIForm
from app.auth.forms import ChangePasswordForm, CreateAccountForm
from app import git_scan
from flask import Markup
import yaml

WORKSPACE_PATH = os.environ.get('WORKSPACE_PATH') or '/home/ville/projects/ad_hoc/workspace/'


@bp.route('/')
@bp.route('/index')
@login_required
def index():
    """ Dashboard """
    metadata = git_scan.collect_environment_metadata(WORKSPACE_PATH)
    return render_template('index.html', title='Dashboard', metadata=metadata)


@bp.route('/user/<username>', methods=['GET', 'POST'])
@login_required
def user(username):
    """ User page, used for changing user password """
    account = User.query.filter_by(username=username).first_or_404()

    password_form = ChangePasswordForm()
    if account.admin is True:
        create_account_form = CreateAccountForm()
    else:
        create_account_form = None
    gitlab_form = ChangeGitLabURIForm()
    gitlab_form.uri_to_gitlab.data = 'http://127.0.0.1:5002'
    if password_form.validate_on_submit():
        account.set_password(password_form.new_password1.data)
        db.session.add(account)
        db.session.commit()
        flash('Password changed')
        return render_template('user.html', title='User settings',
                               password_form=password_form,
                               create_account_form=create_account_form,
                               gitlab_form=gitlab_form)

    if gitlab_form.validate_on_submit():
        flash('Path to GitLab changed')

    if create_account_form.validate_on_submit():
        new_user = User(username=create_account_form.username.data,
                        admin=create_account_form.admin.data,
                        force_password_change=create_account_form.force_password_change.data)
        new_user.set_password(create_account_form.new_password1.data)
        db.session.add(new_user)
        db.session.commit()
        flash('Created user {}'.format(new_user.username))
        return render_template('user.html', title='User settings',
                               password_form=password_form,
                               create_account_form=create_account_form,
                               gitlab_form=gitlab_form)

    return render_template('user.html', title='User settings',
                           password_form=password_form,
                           create_account_form=create_account_form,
                           gitlab_form=gitlab_form)


@bp.route('/samples')
@login_required
def samples():
    """ Page listing all samples in environment """
    metadata = git_scan.collect_environment_metadata(WORKSPACE_PATH)
    return render_template('samples.html', metadata=metadata)


@bp.route('/samples/<sample_hash>')
@login_required
def sample(sample_hash):
    """ Page for single samples """
    metadata = git_scan.collect_environment_metadata(WORKSPACE_PATH)

    sample, sample_tools, sample_results = git_scan.get_sample_by_hash(metadata, sample_hash)

    return render_template('sample.html', sample=sample, tools=sample_tools, results=sample_results)


@bp.route('/samples/<sample_hash>/download')
@login_required
def sample_download(sample_hash):
    """ Downloading samples to local system """
    metadata = git_scan.collect_environment_metadata(WORKSPACE_PATH)
    try:
        sample, _, _ = git_scan.get_sample_by_hash(metadata, sample_hash)
        sample_path = sample['file_path']
    except TypeError:
        pass
    try:
        return send_file(sample_path, as_attachment=True)
    except Exception as e:
        print(e)


@bp.route('/results')
@login_required
def results():
    """ Page for all the results (artefacts and reports) found in environment """
    metadata = git_scan.collect_environment_metadata(WORKSPACE_PATH)
    return render_template('results.html', metadata=metadata)


@bp.route('/results/<result_hash>')
@login_required
def result(result_hash):
    """ Page for single results (artefacts and reports) """
    metadata = git_scan.collect_environment_metadata(WORKSPACE_PATH)

    result, sample, tool = git_scan.get_result_by_hash(metadata, result_hash)

    return render_template('result.html', result=result, sample=sample, tool=tool)


@bp.route('/samples/<result_hash>/download')
@login_required
def result_download(result_hash):
    """ Downloading results to local system """
    metadata = git_scan.collect_environment_metadata(WORKSPACE_PATH)
    try:
        result, sample, tool = git_scan.get_result_by_hash(metadata, result_hash)
        result_path = result['file_path']
    except TypeError:
        pass
    try:
        return send_file(result_path, as_attachment=True)
    except Exception as e:
        print(e)


@bp.route('/tools')
@login_required
def tools():
    """ Page for all the tools (tool scripts and pipelines) found in environment """
    metadata = git_scan.collect_environment_metadata(WORKSPACE_PATH)
    tools, samples, results = metadata['tools'], metadata['samples'], metadata['results']
    return render_template('tools.html', tools=tools, samples=samples, results=results, pipelines=[])


@bp.route('/tools/<tool_name>')
@login_required
def tool(tool_name):
    """ Page for single tools """
    metadata = git_scan.collect_environment_metadata(WORKSPACE_PATH)
    tool, samples, results = git_scan.get_tool_by_name(metadata, tool_name)

    return render_template('tool.html', tool=tool, samples=samples, results=results)

@bp.route('/pipelines/<pipeline_name>')
@login_required
def pipeline(pipeline_name):
    """ Page for single pipelines """
    metadata = git_scan.collect_environment_metadata(WORKSPACE_PATH)

    pipeline_yaml = yaml.load("".join(metadata['contents']), Loader=yaml.SafeLoader)

    """render = '<p><div class="container"><div class="row">'
    job_count = len(pipeline_yaml['jobs'])
    columns = job_count * 2 + 1
    print("columns: ", 12 / columns)
    for i, job in enumerate(pipeline_yaml['jobs']):
        input = job['plan'][0]['get']
        task = job['plan'][1]['task']
        output = job['plan'][2]['put']
        render += '<div class="panel panel-default panel-warning col-sm-1">{}</div>'.format(input)
        render += '<div class="col-sm-1"><span class="glyphicon glyphicon-arrow-right"></span></div>'
        render += '<div class="panel panel-default panel-info col-sm-1">{}</div>'.format(task)
        render += '<div class="col-sm-1"><span class="glyphicon glyphicon-arrow-right"></span></div>'
        if i == len(pipeline_yaml['jobs'])-1:
            render += '<div class="panel panel-default panel-warning col-sm-1">{}</div>'.format(output)

    render = Markup(render)"""

    return render_template('pipeline.html', metadata=metadata, pipeline_name=pipeline_name) #, render=render)