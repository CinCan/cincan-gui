from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField



class ChangeGitLabURIForm(FlaskForm):
    """ Form for setting path to GitLab repository """
    uri_to_gitlab = StringField('GitLab URI')

    submit = SubmitField('Change GitLab URI')
